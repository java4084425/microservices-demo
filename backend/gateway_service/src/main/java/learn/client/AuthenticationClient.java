package learn.client;

import learn.dto.request.IntrospectRequest;
import learn.dto.response.IntrospectResponse;
import learn.dto.response.JsonResponse;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.PostExchange;
import reactor.core.publisher.Mono;

@Repository
public interface AuthenticationClient {
    @PostExchange(url = "/authentication/auth/introspect", contentType = MediaType.APPLICATION_JSON_VALUE)
    Mono<JsonResponse<IntrospectResponse>> introspect(@RequestBody IntrospectRequest request);
}
