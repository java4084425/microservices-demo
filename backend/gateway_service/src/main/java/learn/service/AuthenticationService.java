package learn.service;

import learn.dto.request.IntrospectRequest;
import learn.dto.response.IntrospectResponse;
import learn.dto.response.JsonResponse;
import learn.client.AuthenticationClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final AuthenticationClient authenticationClient;

    public Mono<JsonResponse<IntrospectResponse>> introspect(String token) {
        return authenticationClient.introspect(IntrospectRequest.builder().token(token).build());
    }
}
