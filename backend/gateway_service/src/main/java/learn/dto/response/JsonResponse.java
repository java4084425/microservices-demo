package learn.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import learn.model.EStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonResponse<T> {
    @Builder.Default
    private EStatus status = EStatus.success;

    private String message;

    private T data;
}
