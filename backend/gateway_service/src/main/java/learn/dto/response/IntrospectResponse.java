package learn.dto.response;

import lombok.Data;

@Data
public class IntrospectResponse {
    private boolean valid;
}
