const ProfileModel = require('../models/profile.model');
const {successResponse, failureResponse} = require("./response");

exports.index = async (req, res) => {
    try {
        const existingProfile = await ProfileModel.find({});
        return successResponse(res, 200, existingProfile);
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.show = async (req, res) => {
    try {
        const user_id = req.params.user_id;
        const existingProfile = await ProfileModel.findOne({user_id});
        if (existingProfile) {
            return successResponse(res, 200, existingProfile);
        } else {
            return failureResponse(res, 404, 'Profile not found');
        }
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.create = async (req, res) => {
    try {
        const profile = new ProfileModel({
            user_id: req.body.user_id,
            name: req.body.name,
            birthday: req.body.birthday,
            phone: req.body.phone,
            email: req.body.email,
            address: req.body.address,
        });
        await profile.save();
        return successResponse(res, 201, profile);
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.update = async (req, res) => {
    try {
        const user_id = req.params.user_id;
        const existingProfile = await ProfileModel.findOne({user_id});
        if (!existingProfile) {
            return failureResponse(res, 404, 'Profile not found');
        }
        existingProfile.name = req.body.name;
        existingProfile.birthday = req.body.birthday;
        existingProfile.phone = req.body.phone;
        existingProfile.email = req.body.email;
        existingProfile.address = req.body.address;
        await existingProfile.save();
        return successResponse(res, 200, existingProfile);
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.delete = async (req, res) => {
    try {
        const user_id = req.params.user_id;
        await ProfileModel.deleteOne({user_id});
        return successResponse(res, 200, 'Profile Deleted Successfully');
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}