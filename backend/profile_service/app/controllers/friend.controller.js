const FriendModel = require('../models/friend.model');
const {successResponse, failureResponse} = require("./response");

exports.listFriends = async (req, res) => {
    try {
        const source_id = req.profile_id;
        const listFriends = await FriendModel.find({source_id});
        return successResponse(res, 200, listFriends);
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.requireFriend = async (req, res) => {
    try {
        const source_id = req.profile_id;
        const target_id = req.params.target_id;
        const friend = new FriendModel({source_id, target_id});
        await friend.save();
        return successResponse(res, 200, friend);
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.acceptFriend = async (req, res) => {
    try {
        const source_id = req.profile_id;
        const target_id = req.params.target_id;
        const existingFriend = await FriendModel.findOne({source_id: target_id, target_id: source_id});
        if (!existingFriend) {
            return failureResponse(res, 404, 'Friend not found');
        }
        existingFriend.accepted = true;
        const friend = new FriendModel({source_id, target_id, accepted: true});
        await existingFriend.save();
        await friend.save();
        return successResponse(res, 200, friend);
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}

exports.deleteFriend = async (req, res) => {
    try {
        const source_id = req.profile_id;
        const target_id = req.params.target_id;
        await FriendModel.deleteMany({source_id: source_id, target_id: target_id});
        await FriendModel.deleteMany({source_id: target_id, target_id: source_id})
        return successResponse(res, 200, 'Friends deleted successfully');
    } catch (error) {
        return failureResponse(res, 500, error.message);
    }
}