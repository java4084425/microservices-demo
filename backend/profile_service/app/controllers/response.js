exports.successResponse = function(res, statusCode, data) {
    res.status(statusCode).send({
        status: 'success',
        data: data
    });
};

exports.failureResponse = function(res, statusCode, error) {
    res.status(statusCode).send({
        status: 'error',
        error: error
    });
};