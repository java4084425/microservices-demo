const express = require('express');
const router = express.Router();

const profileController = require('../controllers/profile.controller');

router.get('/', profileController.index);
router.get('/:user_id', profileController.show);
router.post('/', profileController.create);
router.put('/:user_id', profileController.update);
router.delete('/:user_id', profileController.delete);

module.exports = router;