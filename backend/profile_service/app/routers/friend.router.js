const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/auth.middleware');
const friendController = require('../controllers/friend.controller');

router.get('/', [authMiddleware.verifyProfile], friendController.listFriends);
router.post('/:target_id', [authMiddleware.verifyProfile], friendController.requireFriend)
router.put('/:target_id', [authMiddleware.verifyProfile], friendController.acceptFriend);
router.delete('/:target_id', [authMiddleware.verifyProfile], friendController.deleteFriend);

module.exports = router;