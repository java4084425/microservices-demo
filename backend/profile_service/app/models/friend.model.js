const mongoose = require('mongoose');

const friendSchema = new mongoose.Schema(
    {
        source_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'profiles'
        },
        target_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'profiles'
        },
        accepted: {
            type: Boolean,
            default: false
        }
    },
    {timestamps: true}
);

friendSchema.index({source_id: 1, target_id: 1}, {unique: true});

module.exports = mongoose.model('friends', friendSchema);