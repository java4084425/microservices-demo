const mongoose = require('mongoose');

const profileSchema = new mongoose.Schema(
    {
        user_id: {
            type: String,
            required: true,
            unique: true
        },
        name: {
            type: String,
            required: true,
            trim: true,
            minlength: 2,
            maxlength: 50
        },
        birthday: {
            type: Date
        },
        phone: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            minlength: 6,
            maxlength: 15
        },
        email: {
            type: String,
            email: true,
            trim: true
        },
        address: {
            type: String,
            trim: true,
            minlength: 2,
            maxlength: 250
        },
    },
    {timestamps: true}
);

module.exports = mongoose.model('profiles', profileSchema);