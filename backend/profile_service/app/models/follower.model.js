const mongoose = require('mongoose');

const followerSchema = new mongoose.Schema(
    {
        source_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'profiles'
        },
        target_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'profiles'
        }
    },
    {timestamps: true}
);

module.exports = mongoose.model('followers', followerSchema);