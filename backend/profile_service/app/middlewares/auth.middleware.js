const jwt = require('jsonwebtoken');
const ProfileModel = require('../models/profile.model');

const verifyProfile = async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const user_id = decodedToken.sub;
    const profile = await ProfileModel.findOne({user_id});
    if (profile) {
        req.profile_id = profile._id;
        next();
    }
}

module.exports = {
    verifyProfile
};