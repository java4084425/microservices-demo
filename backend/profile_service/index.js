const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
require('dotenv').config();

mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_URL || 'mongodb://localhost:27017/profile_service_db').then(() => {
    console.log('MongoDB connection established');
}).catch((error) => {
    console.log(error.message);
});

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());

const contextPath = '/profiles';
const profileRouter = require('./app/routers/profile.router');
const friendRouter = require('./app/routers/friend.router');
app.use(`${contextPath}/profiles`, profileRouter);
app.use(`${contextPath}/friends`, friendRouter);

const Eureka = require('eureka-js-client').Eureka;
const eureka = new Eureka({
    instance: {
        app: 'profile-service',
        hostName: ' WindowsPC.mshome.net:profile-service:3000',
        ipAddr: '127.0.0.1',
        statusPageUrl: 'http://localhost:3000',
        port: {
            '$': process.env.PORT || '3000',
            '@enabled': 'true',
        },
        vipAddress: 'profile-service',
        dataCenterInfo: {
            '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
            name: 'MyOwn',
        }
    },
    eureka: {
        host: 'localhost',
        port: process.env.EUREKA_SERVER_PORT || 8761,
        servicePath: '/eureka/apps/'
    }
});
eureka.start(function (error) {
    console.log(error || 'complete');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
