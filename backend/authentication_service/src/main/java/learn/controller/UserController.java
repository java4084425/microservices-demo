package learn.controller;

import jakarta.validation.Valid;
import learn.dto.request.UserRequest;
import learn.dto.response.JsonResponse;
import learn.entity.User;
import learn.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public ResponseEntity<?> getUsers() {
        List<User> users = userService.getUsers();
        JsonResponse<?> jsonResponse = JsonResponse.builder().data(users).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable String id) {
        User user = userService.getUserById(id);
        JsonResponse<?> jsonResponse = JsonResponse.builder().data(user).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody @Valid UserRequest request) {
        User user = userService.createUser(request);
        JsonResponse<?> jsonResponse = JsonResponse.builder().data(user).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@RequestBody @Valid UserRequest request, @PathVariable String id) {
        User user = userService.updateUser(request, id);
        JsonResponse<?> jsonResponse = JsonResponse.builder().data(user).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable String id) {
        userService.deleteUser(id);
        JsonResponse<?> jsonResponse = JsonResponse.builder().data(null).build();
        return ResponseEntity.ok(jsonResponse);
    }
}
