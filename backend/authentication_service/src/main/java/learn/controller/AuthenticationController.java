package learn.controller;

import com.nimbusds.jose.JOSEException;
import jakarta.validation.Valid;
import learn.dto.request.LoginRequest;
import learn.dto.request.RegisterRequest;
import learn.dto.request.TokenRequest;
import learn.dto.response.IntrospectResponse;
import learn.dto.response.JsonResponse;
import learn.dto.response.LoginResponse;
import learn.entity.User;
import learn.model.Message;
import learn.model.Statistic;
import learn.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Date;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequest request) {
        LoginResponse response = authenticationService.login(request);
        JsonResponse<?> jsonResponse = JsonResponse.builder().message("Login successful").data(response).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid RegisterRequest request) {
        User user = authenticationService.register(request);
        Statistic statistic = new Statistic("Account " + request.getEmail() + " is created", new Date());
        Message message = new Message(request.getEmail(), request.getName(), "Confirm Email", "Kafka Message");
        kafkaTemplate.send("notification", message);
        kafkaTemplate.send("statistic", statistic);
        JsonResponse<?> jsonResponse = JsonResponse.builder().message("Register successful").data(user).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @PostMapping("/introspect")
    public ResponseEntity<?> introspect(@RequestBody @Valid TokenRequest request) {
        IntrospectResponse response = authenticationService.introspect(request);
        JsonResponse<?> jsonResponse = JsonResponse.builder().message("Introspect successful").data(response).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refresh(@RequestBody @Valid TokenRequest request) throws ParseException, JOSEException {
        LoginResponse response = authenticationService.refreshToken(request);
        JsonResponse<?> jsonResponse = JsonResponse.builder().message("Refresh successful").data(response).build();
        return ResponseEntity.ok(jsonResponse);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(@RequestBody @Valid TokenRequest request) throws ParseException, JOSEException {
        authenticationService.logout(request);
        JsonResponse<?> jsonResponse = JsonResponse.builder().message("Logout successful").build();
        return ResponseEntity.ok(jsonResponse);
    }
}
