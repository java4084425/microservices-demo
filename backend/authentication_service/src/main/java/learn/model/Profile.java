package learn.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Profile {
    @JsonProperty("user_id")
    private String userId;

    private String name;

    private LocalDate birthday;

    private String phone;

    private String email;

    private String address;
}
