package learn.service;

import learn.dto.request.UserRequest;
import learn.entity.Role;
import learn.entity.User;
import learn.exception.NotFoundException;
import learn.repository.RoleRepository;
import learn.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User getUserById(String id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found"));
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User createUser(UserRequest request) {
        if (userRepository.existsByUsername(request.getUsername())) {
            throw new RuntimeException("Username already exists");
        }
        Set<Role> roles = new HashSet<>();
        for (String strRole : request.getRoles()) {
            Role role = roleRepository.findById(strRole).orElseThrow(() -> new NotFoundException("Role not found"));
            roles.add(role);
        }
        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User updateUser(UserRequest request, String id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found"));
        Set<Role> roles = new HashSet<>();
        for (String strRole : request.getRoles()) {
            Role role = roleRepository.findById(strRole).orElseThrow(() -> new NotFoundException("Role not found"));
            roles.add(role);
        }
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(String id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found"));
        userRepository.delete(user);
    }
}
