package learn.client;

import learn.dto.response.ClientResponse;
import learn.model.Profile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "profile-service", url = "${app.services.profile-url}/profiles")
public interface ProfileClient {
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ClientResponse createProfile(@RequestBody Profile profile);

    @GetMapping("/{userId}")
    ClientResponse getProfile(@PathVariable String userId);
}
