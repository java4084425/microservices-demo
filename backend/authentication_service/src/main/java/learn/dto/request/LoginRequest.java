package learn.dto.request;

import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginRequest {
    @Size(min = 2, max = 50)
    private String username;

    @Size(min = 6, max = 50)
    private String password;
}
