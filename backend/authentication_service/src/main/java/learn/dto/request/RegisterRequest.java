package learn.dto.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class RegisterRequest {
    @NotBlank
    @Size(min = 2, max = 50)
    private String username;

    @NotBlank
    @Size(min = 6, max = 50)
    private String password;

    @NotBlank
    @Size(min = 2, max = 100)
    private String name;

    private LocalDate birthday;

    @NotBlank
    @Size(min = 6, max = 15)
    private String phone;

    @Email
    private String email;

    @Size(min = 2, max = 250)
    private String address;
}
