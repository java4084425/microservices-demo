package learn.dto.response;

import lombok.Data;

@Data
public class ClientResponse {
    private String status;

    private Object data;
}
