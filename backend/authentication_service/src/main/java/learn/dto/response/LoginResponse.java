package learn.dto.response;

import learn.entity.User;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginResponse {
    private String token;

    private User user;

    private Object profile;
}
