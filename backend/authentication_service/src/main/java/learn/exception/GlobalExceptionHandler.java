package learn.exception;

import learn.dto.response.JsonResponse;
import learn.model.EStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> exception(Exception e) {
        JsonResponse<?> jsonResponse = JsonResponse.builder().status(EStatus.error).message(e.getMessage()).build();
        return ResponseEntity.internalServerError().body(jsonResponse);
    }

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<?> exception(RuntimeException e) {
        JsonResponse<?> jsonResponse = JsonResponse.builder().status(EStatus.error).message(e.getMessage()).build();
        return ResponseEntity.badRequest().body(jsonResponse);
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<?> exception(NotFoundException e) {
        JsonResponse<?> jsonResponse = JsonResponse.builder().status(EStatus.error).message(e.getMessage()).build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(jsonResponse);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> methodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        Map<String, String> mapErrors = new HashMap<>();
        for (FieldError fieldError : fieldErrors) {
            mapErrors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        JsonResponse<?> jsonResponse = JsonResponse.builder().status(EStatus.error).message("Validation Error").data(mapErrors).build();
        return ResponseEntity.badRequest().body(jsonResponse);
    }
}
