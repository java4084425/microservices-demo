import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import authService from '../services/auth.service';
import {setMessage} from './message';

const user = JSON.parse(localStorage.getItem('user'));

export const register = createAsyncThunk(
    'auth/register',
    async (user, thunkAPI) => {
        try {
            const response = await authService.register(user);
            thunkAPI.dispatch(setMessage(response.data.message));
            return response.data;
        } catch (error) {
            thunkAPI.dispatch(setMessage(error));
            return thunkAPI.rejectWithValue();
        }
    }
);

export const login = createAsyncThunk(
    'auth/login',
    async ({username, password}, thunkAPI) => {
        try {
            const data = await authService.login(username, password);
            return {user: data}
        } catch (error) {
            thunkAPI.dispatch(setMessage(error.message));
            return thunkAPI.rejectWithValue();
        }
    }
);

export const logout = createAsyncThunk(
    'auth/logout',
    async () => {
        await authService.logout();
    }
);

const initialState = user ? {isLoggedIn: true, user} : {isLoggedIn: false, user: null};
const authSlice = createSlice({
    name: 'auth',
    initialState,
    extraReducers: builder => {
        builder
            .addCase(register.fulfilled, (state, action) => {
                state.isLoggedIn = false;
            })
            .addCase(register.rejected, (state, action) => {
                state.isLoggedIn = false;
            })
            .addCase(login.fulfilled, (state, action) => {
                state.isLoggedIn = true;
            })
            .addCase(login.rejected, (state, action) => {
                state.isLoggedIn = false;
                state.user = null;
            })
            .addCase(logout.fulfilled, (state, action) => {
                state.isLoggedIn = false;
                state.user = null;
            });
    }
});
const {reducer} = authSlice;

export default reducer;

