import axios from "axios";

const API_URL = `http://localhost:9090/authentication/auth`;

const register = user => {
    return axios.post(`${API_URL}/register`, user);
};

const login = (username, password) => {
    return axios.post(`${API_URL}/login`, {
        username,
        password
    }).then(response => {
        if (response.data.status === 'success') {
            localStorage.setItem('user', JSON.stringify(response.data.data));
        }
        return response.data.data;
    });
};

const logout = () => {
    localStorage.removeItem('user');
};

const authService = {
    register,
    login,
    logout
};

export default authService;