import {Link, useNavigate} from 'react-router-dom';
import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {clearMessage} from '../slices/message';
import {login} from "../slices/auth";
import {ErrorMessage, Field, Form, Formik} from "formik";

const yup = require('yup');

const LoginPage = () => {
    let navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const {message} = useSelector(state => state.message);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(clearMessage());
    }, [dispatch]);

    const initialValues = {
        username: '',
        password: ''
    };

    const validationSchema = yup.object().shape({
        username: yup.string().required('Username is a required field'),
        password: yup.string().required('Password is a required field'),
    });

    const handleLogin = formValue => {
        setLoading(true);
        const {username, password} = formValue;
        dispatch(login({username, password})).unwrap().then(() => {
            navigate('/');
            window.location.reload();
        }).catch(() => {
            setLoading(false);
        });
    };

    return (
        <div className="d-flex justify-content-center align-items-center mt-5">
            <div className="col-10 col-sm-8 col-md-6 col-lg-4">
                <div className="card">
                    <div className="card-header">
                        <h2>Login to your account</h2>
                    </div>
                    <div className="card-body">
                        <Formik initialValues={initialValues} onSubmit={handleLogin}
                                validationSchema={validationSchema}>
                            <Form>
                                {
                                    message && (
                                        <div className="mt-3">
                                            <div className="alert alert-danger" role="alert">
                                                {message}
                                            </div>
                                        </div>
                                    )
                                }
                                <div className="mt-3">
                                    <label htmlFor="username">Username</label>
                                    <Field name="username" type="text" className="form-control" placeholder="Username"/>
                                    <ErrorMessage name="username" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <label htmlFor="password">Password</label>
                                    <Field name="password" type="password" className="form-control" placeholder="Password"/>
                                    <ErrorMessage name="password" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <button type="submit" disabled={loading} className="btn btn-primary">Login</button>
                                    <Link to="/register">Register</Link>
                                </div>
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LoginPage;