import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {register} from "../slices/auth";
import {ErrorMessage, Field, Form, Formik} from "formik";
import {Link} from "react-router-dom";
import {clearMessage} from "../slices/message";

const yup = require('yup');

const RegisterPage = () => {
    const [successful, setSuccessful] = useState(true);
    const {message} = useSelector(state => state.message);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(clearMessage());
    }, [dispatch]);

    const initialValues = {
        username: '',
        password: '',
        name: '',
        phone: '',
        email: '',
        address: ''
    };

    const validationSchema = yup.object().shape({
        username: yup.string()
            .required('Username is a required field')
            .test('len', 'The username must be between 2 and 50 characters', val =>
                val && val.length >= 2 && val.length <= 50
            ),
        password: yup.string()
            .required('Password is a required field')
            .test('len', 'The password must be between 6 and 50 characters', val =>
                val && val.length >= 6 && val.length <= 50
            ),
        name: yup.string()
            .required('Name is a required field')
            .test('len', 'The name must be between 2 and 100 characters', val =>
                val && val.length >= 2 && val.length <= 100
            ),
        phone: yup.string()
            .required('Phone number is a required field')
            .test('len', 'The phone number must be between 6 and 15 characters', val =>
                val && val.length >= 6 && val.length <= 15
            ),
        email: yup.string()
            .email('Invalid email address'),
        address: yup.string()
    });

    const handleRegister = formValue => {
        const user = formValue;
        setSuccessful(false);
        dispatch(register(user))
            .unwrap()
            .then(() => {
                setSuccessful(true);
            })
            .catch(() => {
                setSuccessful(false);
            });
    };

    return (
        <div className="d-flex justify-content-center align-items-center mt-5">
            <div className="col-10 col-sm-8 col-md-6 col-lg-4">
                <div className="card">
                    <div className="card-header">
                        <h2>Register</h2>
                    </div>
                    <div className="card-body">
                        <Formik initialValues={initialValues} onSubmit={handleRegister}
                                validationSchema={validationSchema}>
                            <Form>
                                {
                                    message && (
                                        <div className="mt-3">
                                            <div className={successful ? "alert alert-success" : "alert alert-danger"}
                                                 role="alert">
                                                {message}
                                            </div>
                                        </div>
                                    )
                                }
                                <div className="mt-3">
                                    <label htmlFor="username">Username</label>
                                    <Field name="username" type="text" className="form-control" placeholder="Username"/>
                                    <ErrorMessage name="username" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <label htmlFor="password">Password</label>
                                    <Field name="password" type="password" className="form-control"
                                           placeholder="Password"/>
                                    <ErrorMessage name="password" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <label htmlFor="name">Name</label>
                                    <Field name="name" type="text" className="form-control" placeholder="Name"/>
                                    <ErrorMessage name="name" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <label htmlFor="phone">Phone</label>
                                    <Field name="phone" type="text" className="form-control" placeholder="Phone"/>
                                    <ErrorMessage name="phone" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <label htmlFor="username">Email</label>
                                    <Field name="email" type="email" className="form-control" placeholder="Email"/>
                                    <ErrorMessage name="email" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <label htmlFor="username">Address</label>
                                    <Field name="address" type="text" className="form-control" placeholder="Address"/>
                                    <ErrorMessage name="address" component="p" className="text-danger"/>
                                </div>
                                <div className="mt-3">
                                    <button type="submit" className="btn btn-primary">Register</button>
                                    <Link to="/login">Login</Link>
                                </div>
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RegisterPage;