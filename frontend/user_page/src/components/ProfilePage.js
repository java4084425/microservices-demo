import {useSelector} from "react-redux";

const ProfilePage = () => {
    const {user: currentUser} = useSelector(state => state.auth);

    return (
        <div className="d-flex justify-content-center align-items-center mt-5">
            <div className="col-10 col-sm-8 col-md-6 col-lg-4">
                <div className="card">
                    <div className="card-header">
                        <h2>Your Profile</h2>
                    </div>
                    <div className="card-body">
                        <p><strong>Name: </strong>{currentUser.profile.name}</p>
                        <p><strong>Birthday: </strong>{currentUser.profile.birthday}</p>
                        <p><strong>Phone: </strong>{currentUser.profile.phone}</p>
                        <p><strong>Email: </strong>{currentUser.profile.email}</p>
                        <p><strong>Address: </strong>{currentUser.profile.address}</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProfilePage;