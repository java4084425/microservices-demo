import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './App.css';
import {useDispatch, useSelector} from 'react-redux';
import {BrowserRouter, Link, Navigate, Route, Routes} from "react-router-dom";
import LoginPage from "./components/LoginPage";
import RegisterPage from "./components/RegisterPage";
import HomePage from "./components/HomePage";
import ProfilePage from "./components/ProfilePage";
import {FaBell, FaFacebook, FaFacebookMessenger, FaUserCircle} from "react-icons/fa";
import {FiLogOut} from "react-icons/fi";
import {useCallback} from "react";
import {logout} from "./slices/auth";

function App() {
    const {user: currentUser} = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const logOut = useCallback(() => {
        dispatch(logout());
    }, [dispatch]);

    return (
        <BrowserRouter>
            {currentUser &&
                <nav className="navbar navbar-expand-lg bg-body-tertiary">
                    <div className="container-fluid">
                        <Link className="navbar-brand" to="/"><FaFacebook className="h1 text-primary"/></Link>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <form className="d-flex" role="search">
                                        <input className="form-control me-2 rounded-5" type="search"
                                               placeholder="Search"
                                               aria-label="Search"/>
                                        <button className="btn btn-outline-primary rounded-5" type="submit">Search
                                        </button>
                                    </form>
                                </li>
                            </ul>
                            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <button className="nav-link" ><FaFacebookMessenger className="h2"/></button>
                                </li>
                                <li className="nav-item">
                                    <button className="nav-link" ><FaBell className="h2"/></button>
                                </li>
                                <li className="nav-item dropdown">
                                    <button className="nav-link dropdown-toggle" role="button"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        <FaUserCircle className="h2 text-primary"/>
                                    </button>
                                    <ul className="dropdown-menu dropdown-menu-end">
                                        <li>
                                            <Link to="/profile" className="dropdown-item">Profile</Link>
                                        </li>
                                        <li>
                                            <hr className="dropdown-divider"/>
                                        </li>
                                        <li>
                                            <button className="dropdown-item" onClick={logOut}><FiLogOut/> Logout</button>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            }
            <Routes>
                <Route path="/" element={currentUser ? <HomePage/> : <Navigate to="/login"/>}/>
                <Route path="/profile" element={currentUser ? <ProfilePage/> : <Navigate to="/login"/>}/>
                <Route path="/login" element={currentUser ? <Navigate to="/"/> : <LoginPage/>}/>
                <Route path="/register" element={currentUser ? <Navigate to="/"/> : <RegisterPage/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
